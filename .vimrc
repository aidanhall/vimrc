" A stand-alone vimrc.

" General Settings {{{1
set nocompatible
filetype plugin on
syntax on

colorscheme industry
set colorcolumn=100,80
set completeopt=menu,preview,longest,menuone
set encoding=utf-8
set hidden
set hlsearch incsearch
set modeline
set modelines=5
set exrc
set secure
set mouse=a
set number relativenumber
set smartcase ignorecase
set title
set wildmenu
set wildmode=longest,list,full
set splitbelow splitright
set path+=**
set completefunc=syntaxcomplete#Complete
" }}}1 Indentation {{{1

set shiftwidth=2
" shiftwidth is used if the value is -1
set softtabstop=-1
" Real tabs may still be inserted with CTRL-V <Tab>
set expandtab
set cindent autoindent
set cinoptions+=j:0l1g0N-
set copyindent

" }}}1 Appearance {{{1
set list
set listchars=tab:\│\ ,trail:-,extends:>,precedes:<,nbsp:+

" }}}1 Plugin Bootstrapping {{{1
let g:my_package_dir = $HOME . "/.vim/after/pack/my-plugins"
let $MY_PACKAGE_DIR = $HOME . "/.vim/after/pack/my-plugins"
let g:my_plugin_repository = "https://gitlab.com/aidanhall/vim-plugins.git"
function! UpdatePlugins()
    echo "Updating Plugins..."
    call system("git -C " . g:my_package_dir . " submodule update --init --recursive")
    echo "Plugins Updated!"
    echo "Updating Help Tags..."
    helptags $MY_PACKAGE_DIR
    echo "Help Tags Updated."
endfunction
command! UpdatePlugins call UpdatePlugins()
command! DownloadPlugins call DownloadPlugins()

function! DownloadPlugins()
    call mkdir(g:my_package_dir, "p")
    echo "Downloading Plugin Repository..."
    call system("git clone " . g:my_plugin_repository . " " . g:my_package_dir)
    echo "Plugins Set Up"
    call UpdatePlugins()
endfunction

function! PluginExists(name)
    let packages_available = getcompletion("", "packadd")
    return index(packages_available, a:name) >= 0
endfunction

" }}}1 Plugins {{{1
packadd! sensible

packadd! autopairs

packadd! dispatch

packadd! fugitive
if PluginExists("cfilter")
    packadd! cfilter
endif

" Standard distribution plugins
packadd! termdebug
packadd! matchit

" }}}1
" }}}1 Status Line {{{1
set laststatus=2
set statusline=%y\ %f%m\ %<%w%r%=\
            \[0x\%02.2B]\ %l/%L,\%03v\ %{mode(\"\ \")}\ [b%n]
" }}}1 Key Bindings {{{1
let mapleader = " "
nnoremap <F2> :term<CR>
nnoremap <F3> :nohlsearch<CR>
nnoremap <F7> :Make<CR><CR>
set cinoptions+=l1

" Filetype Specifics
autocmd filetype gitcommit setlocal colorcolumn+=50
" }}}1
" vim: fdm=marker
