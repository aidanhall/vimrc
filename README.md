# Vimrc
This is a portable `.vimrc` file.
It has the option to also import my
[Vim plugin collection](https://gitlab.com/aidanhall/vim-plugins).

It comes with `DownloadPlugins` and `UpdatePlugins`,
which do what you would expect.
